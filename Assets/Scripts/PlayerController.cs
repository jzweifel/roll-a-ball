﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    public float Speed;
    private int Count;

    public GUIText CountText;
    public GUIText WinText;

    void Start()
    {
        Count = 0;
        SetCountText();
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        rigidbody.AddForce(new Vector3 
        { 
            x = Input.GetAxis("Horizontal") * Speed * Time.deltaTime,
            y = 0 * Speed * Time.deltaTime,
            z = Input.GetAxis("Vertical") * Speed * Time.deltaTime
        });
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Pickup")
        {
            other.gameObject.SetActive(false);
            Count++;
            SetCountText();
        }
    }

    void SetCountText()
    {
        CountText.text = "Count: " + Count.ToString();

        if (Count >= 12)
        {
            WinText.text = "You win!";
            WinText.active = true;
        }
    }
}
